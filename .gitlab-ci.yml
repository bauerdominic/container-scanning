---
variables:
  BUILD_IMAGES: registry.gitlab.com/gitlab-org/security-products/dependencies/build-images
  DOCKERFILE_SOURCE: Dockerfile
  DOCKER_IMAGE: $CI_REGISTRY_IMAGE/tmp/$SCANNER:$CI_COMMIT_SHA
  DEPLOY_REGISTRY_IMAGE: registry.gitlab.com/security-products/container-scanning
  SCANNER: trivy
  DEFAULT_SCANNER: trivy
  GIT_STRATEGY: fetch
  CS_SEVERITY_THRESHOLD: medium

include:
  - local: .gitlab/ci/integration-test.yml
  - local: .gitlab/ci/maintenance.yml
  - local: .gitlab/ci/release.yml
  - local: .gitlab/ci/security.yml
  - local: .gitlab/ci/unit-test.yml
# Workaround to the detached pipeline as described in
# https://gitlab.com/gitlab-org/gitlab/-/issues/34756
workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "detached"'
      when: never
    - when: always

stages:
  - initial-test
  - build-image
  - test
  # release Docker images and distro packages
  - release
  # for scheduled pipeline, we release same image everyday to keep
  # vulnerability db updated
  - maintenance

# Used by release.yml, integration-test.yml, unit-test.yml, and here.
.not-on-schedule:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
      when: on_success

# Used by unit-test.yml and release.yml
.only-on-tag:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: $CI_COMMIT_TAG

# Used by release.yml, security.yml, integration-test.yml, maintenance.yml and here.
.scanners-matrix:
  parallel:
    matrix:
      - SCANNER: [trivy, grype]
        # DOCKERFILE_SOURCE defined with `:` is used to indicate the IMAGE_TAG_POSTFIX for the built image.
        DOCKERFILE_SOURCE: [Dockerfile, Dockerfile.fips:fips]

# Used by maintenance.yml and unit-test.yml
.ruby-alpine:
  extends: .not-on-schedule
  image: $BUILD_IMAGES/ruby-2.7-alpine_git_build-base
  before_script:
    - bundle install --quiet

.build-tmp-image:
  extends: .not-on-schedule
  # docker_ruby is not much larger than docker:stable and lives locally on our network
  image: $BUILD_IMAGES/docker-stable_ruby
  stage: build-image
  services:
    - docker:20.10-dind
  before_script:
    - echo "$CI_DEPENDENCY_PROXY_PASSWORD" | docker login $CI_DEPENDENCY_PROXY_SERVER
      --username $CI_DEPENDENCY_PROXY_USER --password-stdin
  script:
    ## For IMAGE_TAG_POSTFIX use string after `:` in DOCKERFILE_SOURCE (Dockerfile.ubi:fips => fips).
    - |
      if [ "$DOCKERFILE_SOURCE" != "Dockerfile" ]; then
        export IMAGE_TAG_POSTFIX="-${DOCKERFILE_SOURCE##*:}"
      fi
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    # ## Use string before `:` from DOCKERFILE_SOURCE (Dockerfile.ubi:fips => Dockerfile.ubi) to build the image.
    - docker build --network host --build-arg SCANNER -f "${DOCKERFILE_SOURCE%%:*}"
      -q -t $DOCKER_IMAGE$IMAGE_TAG_POSTFIX .
    - docker push $DOCKER_IMAGE$IMAGE_TAG_POSTFIX

build-scanner-image:
  extends:
    - .build-tmp-image
    - .scanners-matrix

check version:
  extends: .only-on-tag
  image: $BUILD_IMAGES/docker-stable_ruby
  stage: initial-test
  script: ./script/check_version
